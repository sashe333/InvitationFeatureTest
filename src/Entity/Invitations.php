<?php

namespace App\Entity;

use App\Repository\InvitationsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InvitationsRepository::class)
 */
class Invitations
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Users.php")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $userId;

    /**
     * @ORM\Column(name="sender_id", type="integer")
     */
    private $senderId;

    /**
     * @ORM\Column(name="invitation_status", type="string", length=50)
     */
    private $invitationStatus;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getSenderId() : int
    {
        return $this->senderId;
    }

    /**
     * @param int $senderId
     *
     * @return self
     */
    public function setSenderId(int $senderId) : self
    {
        $this->senderId = $senderId;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvitationStatus() : string
    {
        return $this->invitationStatus;
    }

    /**
     * @param string $invitationStatus
     * @return self
     */
    public function setInvitationStatus(string $invitationStatus): self
    {
        $this->invitationStatus = $invitationStatus;

        return $this;
    }

}
