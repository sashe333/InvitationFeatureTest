<?php


namespace App\Controller;


use App\Entity\Invitations;
use App\Entity\Users;
use App\Form\Type\UserType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserInvitationsController extends AbstractApiController
{
    /** @var ManagerRegistry $doctrine */
    private $doctrine;

    private $doctrineManager;

    /**
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrineManager = $doctrine->getManager();
        $this->doctrine = $doctrine;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexActionUser(Request $request): Response
    {
        return $this->json($this->doctrine->getRepository(Users::class)->findAll());
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(UserType::class);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Users $user */
        $user = $form->getData();

        $this->doctrine->getManager()->persist($user);
        $this->doctrine->getManager()->flush();

        return $this->json($user);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexActionInvitation(Request $request): Response
    {
        return $this->json($this->doctrine->getRepository(Invitations::class)->findAll());
    }

    /**
     * @param int $toUser
     * @return Response
     */
    public function sendInvitation(int $toUser): Response
    {
        $loggedInUserId = 2;
        if ($loggedInUserId === $toUser) {
            return $this->respond(Response::HTTP_FORBIDDEN);
        }

        /** @var Users $receivingUser */
        $receivingUser = $this->doctrine->getRepository(Users::class)->findBy(array("id" => $toUser));

        $invitation = new Invitations();

        $invitation->setSenderId($loggedInUserId);
        $invitation->setInvitationStatus("Pending");

        $receivingUser->addInvitation($invitation);

        $this->doctrineManager->persist($receivingUser);
        $this->doctrineManager->flush();

        return $this->json($this->doctrine->getRepository(Invitations::class)->findBy(array("sender_id" => $loggedInUserId)));
    }

    /**
     * @param int $fromUser
     * @return Response
     */
    public function acceptInvitation(int $fromUser): Response
    {
        $loggedInUserId = 3;
        try
        {
            /** @var Invitations $invitation */
            $invitation = $this->doctrine->getRepository(Invitations::class)->findOneBy(array("sender_id" => $fromUser, "user_id" => $loggedInUserId));
        } catch (\Throwable $exception)
        {
            return $this->respond(Response::HTTP_NOT_FOUND);
        }

        try
        {
            /** @var Users $receivingUser */
            $receivingUser = $this->doctrine->getRepository(Users::class)->findBy(array("id" => $loggedInUserId));
        } catch (\Throwable $exception)
        {
            return $this->respond(Response::HTTP_NOT_FOUND);
        }

        $invitation->setInvitationStatus("Accepted");

        $receivingUser->setInvitation(new ArrayCollection([$invitation]));

        $this->doctrineManager->persist($receivingUser);
        $this->doctrineManager->flush();

        return $this->json($this->doctrine->getRepository(Invitations::class)->findBy(array("user_id" => $loggedInUserId)));
    }

    /**
     * @param int $fromUser
     * @return Response
     */
    public function DeclineInvitation(int $fromUser): Response
    {
        $loggedInUserId = 3;
        try
        {
            /** @var Invitations $invitation */
            $invitation = $this->doctrine->getRepository(Invitations::class)->findOneBy(array("sender_id" => $fromUser, "user_id" => $loggedInUserId));
        } catch (\Throwable $exception)
        {
            return $this->respond(Response::HTTP_NOT_FOUND);
        }

        try
        {
            /** @var Users $receivingUser */
            $receivingUser = $this->doctrine->getRepository(Users::class)->findBy(array("id" => $loggedInUserId));
        } catch (\Throwable $exception)
        {
            return $this->respond(Response::HTTP_NOT_FOUND);
        }

        $invitation->setInvitationStatus("Declined");

        $receivingUser->setInvitation(new ArrayCollection([$invitation]));

        $this->doctrineManager->persist($receivingUser);
        $this->doctrineManager->flush();

        return $this->json($this->doctrine->getRepository(Invitations::class)->findBy(array("user_id" => $loggedInUserId)));
    }

}